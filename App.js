/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from "react";
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Geolocation from '@react-native-community/geolocation';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [pos, setPos] = React.useState({
    lat: null,
    lgn: null,
    speed: null,
    distance: null,
  })


  React.useEffect(() => {
    Geolocation.watchPosition(info => setPos({lat: info.coords.latitude, lng: info.coords.longitude, speed: info.coords.speed, distance: info.coords.altitude}), error => console.log(error), {timeout : 1000, enableHighAccuracy: true});
  }, [])
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View
          style={{backgroundColor: isDarkMode ? Colors.black : Colors.white, textAlign: 'center', marginHorizontal: 50}}>

          <Text>{pos.lat} latitute</Text>
          <Text>{pos.lng} longitute</Text>
          <Text>{pos.speed} speed</Text>
          <Text>{pos.distance} distance</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
